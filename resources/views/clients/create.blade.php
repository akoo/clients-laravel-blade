@extends('layouts.app')

@section('content')
<h1>Creer un nouveau client</h1>

<form action="{{ route('clients.store') }}" method="POST">
    @include('includes/form')
    <button type="submit" class="btn btn-primary">Ajouter le client</button>

</form>

</ul>

@endsection