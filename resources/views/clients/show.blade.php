@extends ('layouts.app')

@section('content')

<h1>{{ $client->name }}</h1>
<a href="{{ route('clients.edit', ['client' => $client->id]) }}" class="btn btn-secondary my-3">Editer</a>
<form action="{{ route('clients.destroy', ['client' => $client->id]) }}" method="POST" style="display: inline;">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger">Supprimer</button>
<hr>
<p><strong>Email :</strong> {{ $client->email }}</p>
<p><strong>Entreprise :</strong> {{ $client->entreprise->name }}</p>






@endsection